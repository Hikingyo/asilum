var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    var session = req.session;
    if(typeof(session.userName) == 'undefined'){
        session.userName = 'JohnDoe';
    }
    res.render('index', {title: 'ASilum', userName: session.userName});
});

module.exports = router;
