/**
 * Gulp tasks config
 */

var public = "./public";
var views = "./views";
var bin = "./bin";

module.exports = {
    nodemon: {
        script : bin + "/www",
        env : { 'NODE_ENV': 'DEBUG=tchat:*'}
    },
    browsersync: {
        proxy : 'http://localhost:3000',
        port: '3030',
        files : [public + "/**"]
    },
    styles: {
        src: public + "/stylesheets"
    },
    scripts: {
        src: public + "/javascripts"
    },
    html: {
        src: views
    }
}