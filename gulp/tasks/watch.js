/**
 * Created by Isabelle on 05/12/2015.
 */
var gulp = require('gulp');
var config = require('../config');
var reload = require('browser-sync').reload;

gulp.task('watch',['browsersync'], function(){
   gulp.watch(config.styles.src + '/*.less' ).on('change', reload);
    gulp.watch(config.scripts.src + '/**/*.js').on('change', reload);
    gulp.watch(config.html.src + '/**/*.ejs').on('change', reload);
});