/**
 * Created by Isabelle on 05/12/2015.
 */
var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var $ = gulpLoadPlugins();
var config = require('../config').nodemon;

gulp.task('nodemon', function (cb) {
    var started = false;
    console.log(started);
    return $.nodemon({
            script: config.script,
            env: config.env,
            watch: ['./bin/www', './app'],
            ignoreRoot: ['./views']
        })
        .on('start', function () {
            //to avoid multiple start
            if (!started) {
                cb();
                started : true;
                console.log('node server started !');
            }
        })
});