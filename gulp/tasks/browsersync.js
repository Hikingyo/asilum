/**
 * Created by Isabelle on 05/12/2015.
 */
var gulp = require('gulp');
var config = require('../config').browsersync;
var browsersync = require('browser-sync');

gulp.task('browsersync', function(){
    browsersync.init(null, {
        proxy : config.proxy,
        port : config.port
    })
});