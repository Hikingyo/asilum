/**
 * Created by hikingyo on 08/12/2015.
 */
/**
 * Messag parser
 * Analyze String from client
 * @require lodash
 * @param message the string from client
 * @return object { type, content }
 */
var _ = require('lodash');

/**
 * 4L Words collection
 * @type {string[]}
 */
var forbiddenWords = [
    'salope',
    'pute',
    'putain',
    'connard',
    'enculé',
    'enculée',
    'enculer',
    'connasse',
    'nique',
    'niquer',
    'niqué',
    'niquée'
];

exports.analyse = function (message){
    if(typeof message == 'undefined'){
        return;
    }
    var type = null;
    var content;
    // If message is too long
    if(message.length > 140){
        type = 'emit';
        content = 'Your message is over than 140 characters';
    }
    // use of 4L words ?
    else if(is4LWords(message)){
        type = 'emit';
        content = 'Hey, thou hast believed in Granny ?';
    }
    // use of commandes
/*    else if(_.startsWith(message, '/')){
        commande = analyzeCommand(message);
        type = commande.type;
        content = commande.action;
    }*/
    // Prepping message and sending
    else{
        type = 'broadcast';
        content = urlToLink(_.escape(_.trim(message)));
    }

    return {'type': type, 'content': content};
};

function is4LWords(message){
    var match = false;
    _.map(forbiddenWords, function(word){
        if(_.includes(message, word)){
            match = true;
        }
    });
    return match;
}

function analyzeCommand(data){
    console.log('not implemented yet !!');
}

function urlToLink(message){
    var regex = /(https?:\/\/(www\.)?(\w*\.?\w*)*\.\w{2,16}[%(\/\w*)]*(\.\w{2,16})?(\?\w+=\w+)*\/?)/g;
    var message = message.replace(regex, "<a href=\"$1\">$1</a>");
    return message;
}